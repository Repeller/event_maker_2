﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using EventMaker2.Common;
using EventMaker2.Model;

namespace EventMaker2.Viewmodel
{
	class CreateEventVM : INotifyPropertyChanged
	{


		public string Title { get; set; }
		public string Place { get; set; }
		public TimeSpan Start { get; set; }
		public TimeSpan End { get; set; }
		public ICommand Commander { get; set; }

		//private string _feedback;

		public string Feedback
		{
			get => _feedback;
			set
			{ 
				_feedback = value;
				OnPropertyChanged(Feedback);
			}

}

		public EventCatalogSingleton Catalog = EventCatalogSingleton.Instance;
		private string _feedback;

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

		public CreateEventVM()
		{
			Title = "test";
			Place = "test";
			Commander = new Relaycommand(CreateEvent);
			Feedback = "test";
		}
		
		public void CreateEvent()
		{
			Feedback = "what the fuck";
		}
	}
}
