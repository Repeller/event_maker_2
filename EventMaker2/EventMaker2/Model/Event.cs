﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventMaker2.Model
{
	public class Event
	{
		private static int IdCounter = 0;

		public int Id { get; set; }
		public string Name { get; set; }
		public string Place { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }

		public Event(string name, string place, DateTime start, DateTime end)
		{
			IdCounter++;
			Id = IdCounter;

			Name = name;
			Place = place;
			Start = start;
			End = end;
		}
	}
}
