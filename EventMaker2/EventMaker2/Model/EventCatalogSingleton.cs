﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventMaker2.Model
{
	public class EventCatalogSingleton
	{
		public ObservableCollection<Event> Events = new ObservableCollection<Event>();

		private static EventCatalogSingleton instance = null;

		static public EventCatalogSingleton Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new EventCatalogSingleton();
				}
				return instance;
			}
		}
		private EventCatalogSingleton()
		{
			Load();
		}
		public void Load()
		{
			Events.Add(
				new Event("test2", "hell",
					new DateTime(2019, 2, 1, 12, 0, 0),
					new DateTime(2019, 2, 1, 14, 0, 0)));
			Events.Add(
				new Event("test45", "hell",
					new DateTime(2019, 3, 1, 12, 0, 0),
					new DateTime(2019, 3, 1, 14, 0, 0)));
			Events.Add(
				new Event("test232", "hell",
					new DateTime(2019, 4, 1, 12, 0, 0),
					new DateTime(2019, 4, 1, 14, 0, 0)));
			Events.Add(
				new Event("test12", "hell",
					new DateTime(2019, 5, 1, 12, 0, 0),
					new DateTime(2019, 5, 1, 14, 0, 0)));
			Events.Add(
				new Event("test32", "hell",
					new DateTime(2019, 6, 1, 12, 0, 0),
					new DateTime(2019, 6, 1, 14, 0, 0)));
		}
	}
}
